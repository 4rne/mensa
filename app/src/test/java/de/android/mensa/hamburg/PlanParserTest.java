package de.android.mensa.hamburg;

import android.os.Build;
import android.support.annotation.RequiresApi;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import de.android.mensa.hamburg.materials.Allergenic;
import de.android.mensa.hamburg.materials.Dish;
import de.android.mensa.hamburg.tools.PlanParser;

import static java.util.concurrent.ThreadLocalRandom.current;
import static junit.framework.Assert.assertEquals;

public class PlanParserTest {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Test
    public void testCreateURL() {
        assertEquals("http://speiseplan.studierendenwerk-hamburg.de/de/0/0/0/", PlanParser.createURL(0, 0, "0", "de"));
        assertEquals("http://speiseplan.studierendenwerk-hamburg.de/de/0/0/99/", PlanParser.createURL(1, 0, "0", "de"));
        int randomDay = current().nextInt(1, Integer.MAX_VALUE);
        assertEquals("http://speiseplan.studierendenwerk-hamburg.de/de/0/0/99/", PlanParser.createURL(randomDay, 0, "0", "de"));
    }

    @Test
    public void testRemoveBrackets() {
        assertEquals("Test", PlanParser.removeBracketsAndRedundantWhitespace("Test()"));
        assertEquals("Test", PlanParser.removeBracketsAndRedundantWhitespace("Test() "));
        assertEquals("Test ,", PlanParser.removeBracketsAndRedundantWhitespace("Test() ,"));
        assertEquals("Test,", PlanParser.removeBracketsAndRedundantWhitespace("Test(), "));
        assertEquals("Test,", PlanParser.removeBracketsAndRedundantWhitespace("Test  , "));
        assertEquals("Currybratwurst, mit verschiedenen Grillsoßen, Pommes Frites",
                PlanParser.removeBracketsAndRedundantWhitespace("Currybratwurst (3,4,8,19,20,23) , mit verschiedenen Grillsoßen (2,9,22,23) , Pommes Frites "));
    }

    @Test
    public void testParse() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        String sampleDishHtml;
        try (InputStream inputStream = classLoader.getResourceAsStream("sample_dish_from_website.html")) {
            sampleDishHtml = readFromInputStreamWithoutLinebreak(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Document testDocument = Jsoup.parse(sampleDishHtml);
        ArrayList<Dish> dishTestList = PlanParser.ParseResult(testDocument);

        Dish testExpectedDish1 = new Dish("3 Stück Kartoffelpuffer, Apfelkompott","1,40 €","2,60 €","3,25 €");
        testExpectedDish1.addAllergenic(new Allergenic("Glutenhaltiges getreide",14, R.drawable.wheat));
        testExpectedDish1.addAllergenic(new Allergenic("Ei/-erzeugnisse",16, R.drawable.eggs));
        testExpectedDish1.addAllergenic(new Allergenic("Antioxidationsmittel",3));

        Dish testExpectedDish2 = new Dish("2 pancakes, berry compote","1,40 €","2,60 €","3,25 €");
        testExpectedDish2.addAllergenic(new Allergenic("(grain) contains gluten",14, R.drawable.wheat));
        testExpectedDish2.addAllergenic(new Allergenic("eggs/egg products",16, R.drawable.eggs));
        testExpectedDish2.addAllergenic(new Allergenic("milk/milk products (including lactose)",20, R.drawable.milk));
        testExpectedDish2.addAllergenic(new Allergenic("colours",1));
        testExpectedDish2.addAllergenic(new Allergenic("(artificial) sweetener",9));
        testExpectedDish2.addAllergenic(new Allergenic("milk/milk products (including lactose)",20, R.drawable.milk));

        assertEquals(2,dishTestList.size());
        assertEquals(testExpectedDish1,dishTestList.get(0));
        assertEquals(testExpectedDish2,dishTestList.get(1));
    }

    private String readFromInputStreamWithoutLinebreak(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line);
            }
        }
        return resultStringBuilder.toString();
    }

}
