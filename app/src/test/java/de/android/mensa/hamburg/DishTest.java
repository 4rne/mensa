package de.android.mensa.hamburg;

import org.junit.Test;

import de.android.mensa.hamburg.materials.Allergenic;
import de.android.mensa.hamburg.materials.Dish;
import nl.jqno.equalsverifier.EqualsVerifier;

import static org.junit.Assert.assertEquals;

public class DishTest {

    @Test
    public void simpleTest() {
        Dish testDish = new Dish("testDish", "1,30 €", "2,50 €", "3,25 €");
        assertEquals("testDish", testDish.getDescription());
        assertEquals("1,30 €", testDish.getPriceStudentsString());
        assertEquals(130, testDish.getPriceStudents());
        assertEquals("2,50 €", testDish.getPriceAttendantsString());
        assertEquals(250, testDish.getPriceAttendants());
        assertEquals("3,25 €", testDish.getPriceGuestsString());
        assertEquals(325, testDish.getPriceGuests());
    }

    @Test
    public void priceOnlyInEuroWithEuroSymbol() {
        Dish testDish = new Dish("testDish", "2 €");
        assertEquals("2 €", testDish.getPriceStudentsString());
        assertEquals(0, testDish.getPriceAttendants());
    }

    @Test
    public void priceOnlyInEuroWithoutEuroSymbol() {
        Dish testDish = new Dish("testDish", "2");
        assertEquals("2", testDish.getPriceStudentsString());
        assertEquals(0, testDish.getPriceAttendants());
    }

    @Test
    public void priceOnlyInEuroWithEuroSymbolAndNegativ() {
        Dish testDish = new Dish("testDish", "-2,50 €");
        assertEquals("-2,50 €", testDish.getPriceStudentsString());
        assertEquals(0, testDish.getPriceAttendants());
    }

    @Test
    public void priceOnlyInEuroWithoutEuroSymbolAndNegativ() {
        Dish testDish = new Dish("testDish", "-2,50");
        assertEquals("-2,50", testDish.getPriceStudentsString());
        assertEquals(0, testDish.getPriceAttendants());
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(Dish.class).withNonnullFields("_allergens").verify();
    }
}