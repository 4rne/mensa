package de.android.mensa.hamburg;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AboutDialogTest {
    @Test
    public void shouldCapitalizeChars() {
        assertEquals("A", AboutDialog.capitalizeFirstCharOfString("a"));
        assertEquals("Z", AboutDialog.capitalizeFirstCharOfString("z"));
    }

    @Test
    public void shouldNotCapitalizeCapitalizedChars() {
        assertEquals("A", AboutDialog.capitalizeFirstCharOfString("A"));
        assertEquals("Z", AboutDialog.capitalizeFirstCharOfString("Z"));
    }

    @Test
    public void shouldCapitalizeStrings() {
        assertEquals("Abc", AboutDialog.capitalizeFirstCharOfString("abc"));
        assertEquals("Zxy", AboutDialog.capitalizeFirstCharOfString("zxy"));
    }

    @Test
    public void shouldNotCapitalizeCapitalizedStrings() {
        assertEquals("Abc", AboutDialog.capitalizeFirstCharOfString("Abc"));
        assertEquals("Zxy", AboutDialog.capitalizeFirstCharOfString("Zxy"));
    }
}
