package de.android.mensa.hamburg.materials

class Canteen(val code: String, val name: String, val shortName: String, var visibility: Boolean)
