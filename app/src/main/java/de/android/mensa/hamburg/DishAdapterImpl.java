package de.android.mensa.hamburg;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.android.mensa.hamburg.materials.Allergenic;
import de.android.mensa.hamburg.materials.Dish;

public class DishAdapterImpl extends RecyclerView.Adapter<DishAdapterImpl.ViewHolder> implements IDishAdapter {
    private final IMainPresenter presenter;
    private final ArrayList<Dish> mDataSet;

    DishAdapterImpl(ArrayList<Dish> dishes, IMainPresenter presenter) {
        this.presenter = presenter;
        mDataSet = dishes;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DishAdapterImpl.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dish, parent, false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your data set at this position
        // - replace the contents of the view with that element
        holder.tvDescription.setText(mDataSet.get(position).getDescription());
        holder.tvPriceStudents.setText(mDataSet.get(position).getPriceStudentsString());
        holder.tvPriceAttendants.setText(mDataSet.get(position).getPriceAttendantsString());
        holder.tvPriceGuests.setText(mDataSet.get(position).getPriceGuestsString());
        holder.tvPriceStudents.setVisibility(presenter.isShowStudentPrice() ? View.VISIBLE : View.GONE);
        holder.tvPriceAttendants.setVisibility(presenter.isShowAttendantPrice() ? View.VISIBLE : View.GONE);
        holder.tvPriceGuests.setVisibility(presenter.isShowGuestPrice() ? View.VISIBLE : View.GONE);
        boolean iconRowVisible = false;
        if (presenter.isShownAllergens()) {
            iconRowVisible = setIconsIfAvailable(holder, position);
        }
        SetMinimumHeightAndVisibilityOfIconRow(holder, iconRowVisible);
    }

    private void SetMinimumHeightAndVisibilityOfIconRow(ViewHolder holder, boolean iconRowVisible){
        int minHeight = (int) presenter.getView().getActivityContext().getResources().getDimension(R.dimen.itemdish_minimal_height);

        if(iconRowVisible){
            holder.iconrow.setVisibility(View.VISIBLE);
            holder.rowContentLayout.setMinimumHeight(minHeight);
        } else {
            holder.iconrow.setVisibility(View.GONE);
            holder.textcontentlayout.setMinimumHeight(minHeight);
        }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    @Override
    public boolean setIconsIfAvailable(ViewHolder holder, int position) {
        holder.iconrow.removeAllViews();
        int size = (int) presenter.getView().getActivityContext().getResources().getDimension(R.dimen.itemdish_icon_size);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(size, size);
        Dish dish = mDataSet.get(position);
        int visibleIcons = 0;
        for (Allergenic tmp : dish.getAllergens()) {
            int iconId = tmp.getIconId();
            if (iconId != 0) {
                ImageView iv = (new AppCompatImageView(presenter.getView().getActivityContext()));
                iv.setImageResource(iconId);
                iv.setLayoutParams(layoutParams);
                holder.iconrow.addView(iv);
                visibleIcons++;
            }
        }
        //Return if any icons have been set
        return (visibleIcons != 0);
    }

    @Override
    public void handleDialogRequest(int itemposition) {
        List<Allergenic> allergens = mDataSet.get(itemposition).getAllergens();
        //Only if there are allergens to show, there should be a Dialog
        if (allergens.size() > 0) {
            presenter.getView().showAlertDialog(setAllergensString(allergens));
        }
    }

    @Override
    public String setAllergensString(List<Allergenic> allergens) {
        StringBuilder sb = new StringBuilder();
        sb.append(allergens.get(0).getName());
        for (int i = 1; i < allergens.size(); i++) {
            sb.append("\n");
            sb.append(allergens.get(i).getName());
        }
        return sb.toString();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tvDescription)
        TextView tvDescription;
        @BindView(R.id.tvPriceStudents)
        TextView tvPriceStudents;
        @BindView(R.id.tvPriceAttendants)
        TextView tvPriceAttendants;
        @BindView(R.id.tvPriceGuests)
        TextView tvPriceGuests;
        @BindView(R.id.iconrow)
        LinearLayout iconrow;
        @BindView(R.id.rowContentLayout)
        LinearLayout rowContentLayout;
        @BindView(R.id.textContentLayout)
        LinearLayout textcontentlayout;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            handleDialogRequest(getAdapterPosition());
        }
    }
}
