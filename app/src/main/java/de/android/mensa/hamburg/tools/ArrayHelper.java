package de.android.mensa.hamburg.tools;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.android.mensa.hamburg.materials.Canteen;

public class ArrayHelper {
    private static final String TAG = "ArrayHelper";
    private final SharedPreferences prefs;

    public ArrayHelper(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Converts the provided ArrayList<String>
     * into a JSONArray and saves it as a single
     * string in the apps shared preferences
     *
     * @param key   Preference key for SharedPreferences
     * @param array ArrayList<Canteen> containing the list items
     */
    public void saveArray(String key, ArrayList<Canteen> array) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key);
        editor.putString(key, convertCanteentoJSON(array).toString());
        editor.apply();
    }

    /**
     * Loads a JSONArray from shared preferences
     * and converts it to an ArrayList<String>
     *
     * @param key Preference key for SharedPreferences
     * @return ArrayList<Canteen> containing the saved values from the JSONArray
     */
    public ArrayList<Canteen> getArray(String key, boolean showInv) {
        String jArrayString = prefs.getString(key, "NOPREFSAVED");
        if (jArrayString.matches("NOPREFSAVED")) return setDefaultArray(key, showInv);
        else {
            try {
                JSONArray jArray = new JSONArray(jArrayString);
                return convertCanteenfromJSON(jArray, showInv);
            } catch (JSONException e) {
                return getDefaultArray();
            }
        }
    }

    /**
     * Changes the visibility of a specific canteen by its code in the given array via the key to visibility
     *
     * @param key        The key of the array which should be loaded
     * @param code       The code as string from the canteen which should be changed
     * @param visibility The desired visibility
     */
    public void setVisibilityOfCanteen(String key, String code, boolean visibility) {
        ArrayList<Canteen> canteens_list = getArray(key, true);
        int indexOfCanteenWithThisCode = getIdOfCanteenWithCode(canteens_list, code);
        if (indexOfCanteenWithThisCode >= 0) {
            canteens_list.get(indexOfCanteenWithThisCode).setVisibility(visibility);
            saveArray(key, canteens_list);
        }
    }

    /**
     * Gets the Position of a canteen with a specific code in an array of canteens
     *
     * @param canteens_list The list of canteens
     * @param code          The code of the Canteen which should be searched after
     * @return The position of the canteens as integer Or -1 if nothing is found
     */
    public int getIdOfCanteenWithCode(ArrayList<Canteen> canteens_list, String code) {
        for (int i = 0; i < canteens_list.size(); i++) {
            if (canteens_list.get(i).getCode().equals(code)) {
                return i;
            }
        }
        Log.d(TAG, "There is no canteen with this code: " + code);
        return (-1);
    }

    private ArrayList<Canteen> setDefaultArray(String key, boolean showInv) {
        String canteens = "[{'code':'590','name':'Armgartstraße','shortname':'Armgart','visibility':false}," +
                "{'code':'520','name':'Bergedorf','shortname':'Berg','visibility':false}," +
                "{'code':'530','name':'Berliner Tor','shortname':'Berlin','visibility':false}," +
                "{'code':'560','name':'Botanischer Garten','shortname':'Bot-G.','visibility':false}," +
                "{'code':'410','name':'Bucerius Law School','shortname':'Law','visibility':false}," +
                "{'code':'660','name':'Alexanderstraße','shortname':'Alex','visibility':false}," +
                "{'code':'690','name':'Mittelweg','shortname':'Mittel','visibility':false}," +
                "{'code':'531','name':'Berliner Tor','shortname':'Berlin','visibility':false}," +
                "{'code':'680','name':'CFEL','shortname':'CFEL','visibility':false}," +
                "{'code':'610','name':'Jungiusstraße','shortname':'Jungiusstraße','visibility':false}," +
                "{'code':'340','name':'Campus','shortname':'Campus','visibility':true}," +
                "{'code':'420','name':'Finkenau','shortname':'Finkenau','visibility':false}," +
                "{'code':'540','name':'Geomatikum','shortname':'Geo','visibility':false}," +
                "{'code':'430','name':'HCU','shortname':'HCU','visibility':false}," +
                "{'code':'570','name':'Harburg','shortname':'Harburg','visibility':false}," +
                "{'code':'350','name':'Philoturm','shortname':'Phil','visibility':true}," +
                "{'code':'310','name':'Studierendenhaus','shortname':'Stud','visibility':true}," +
                "{'code':'580','name':'Informatikum','shortname':'Ikum','visibility':false}]";
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key);
        editor.putString(key, canteens);
        editor.apply();
        try {
            JSONArray jArray = new JSONArray(canteens);
            return convertCanteenfromJSON(jArray, showInv);
        } catch (JSONException e) {
            return getDefaultArray();
        }

    }

    // Get a default array in the event that there is no array
    // saved or a JSONException occurred
    private ArrayList<Canteen> getDefaultArray() {
        ArrayList<Canteen> array = new ArrayList<>();
        array.add(new Canteen("590", "Armgartstraße", "Armgart", true));
        array.add(new Canteen("520", "Bergedorf", "Berg", true));
        array.add(new Canteen("530", "Berliner Tor", "Berlin", true));
        array.add(new Canteen("560", "Botanischer Garten", "Bot-G.", true));
        array.add(new Canteen("410", "Bucerius Law School", "Law", true));
        array.add(new Canteen("660", "Alexanderstraße", "Alex", true));
        array.add(new Canteen("690", "Mittelweg", "Mittel", true));
        array.add(new Canteen("531", "Berliner Tor", "Berlin", true));
        array.add(new Canteen("680", "CFEL", "CFEL", true));
        array.add(new Canteen("610", "Jungiusstraße", "Jungiusstraße", true));
        array.add(new Canteen("340", "Campus", "Campus", true));
        array.add(new Canteen("420", "Finkenau", "Finkenau", true));
        array.add(new Canteen("540", "Geomatikum", "Geo", true));
        array.add(new Canteen("430", "HCU", "HCU", true));
        array.add(new Canteen("570", "Harburg", "Harburg", true));
        array.add(new Canteen("350", "Philoturm", "Phil", true));
        array.add(new Canteen("310", "Studierendenhaus", "Stud", true));
        array.add(new Canteen("580", "Informatikum", "Ikum", true));
        return array;
    }

    private JSONArray convertCanteentoJSON(ArrayList<Canteen> canteens) {
        JSONArray jArray = new JSONArray();
        for (Canteen canteen : canteens) {
            JSONObject tmp = new JSONObject();
            try {
                tmp.put("code", canteen.getCode());
                tmp.put("name", canteen.getName());
                tmp.put("shortname", canteen.getShortName());
                tmp.put("visibility", canteen.getVisibility());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jArray.put(tmp);
        }
        return jArray;
    }

    private ArrayList<Canteen> convertCanteenfromJSON(JSONArray jArray, boolean showInv) {
        ArrayList<Canteen> tmp = new ArrayList<>();
        for (int i = 0; i < jArray.length(); i++) {
            try {
                JSONObject jObject = jArray.getJSONObject(i);
                String code = jObject.getString("code");
                String name = jObject.getString("name");
                String shortname = jObject.getString("shortname");
                Boolean visibility = jObject.getBoolean("visibility");
                if (showInv) {
                    tmp.add(new Canteen(code, name, shortname, visibility));
                } else if (visibility) {
                    tmp.add(new Canteen(code, name, shortname, true));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return tmp;
    }
}