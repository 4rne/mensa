package de.android.mensa.hamburg.tools;

import java.util.ArrayList;
import de.android.mensa.hamburg.materials.Dish;

/**
 * The callback which defines the methods which are called when the planparser has finished
 */
public interface IPlanParserCallback {

    /**
     * The method to be called if there is no menu for the selected day
     */
    void NoMenu();

    /**
     * The method to be called if a list of dishes is received
     * @param dishList the dishlist to be set
     */
    void SetDishes(ArrayList<Dish> dishList);

    /**
     * The method to be called if there was an error while fetching the data
     */
    void ErrorWhileFetching();
}
