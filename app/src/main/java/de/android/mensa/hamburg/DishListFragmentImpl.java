/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package de.android.mensa.hamburg;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.android.mensa.hamburg.materials.Dish;
import de.android.mensa.hamburg.tools.IPlanParserCallback;
import de.android.mensa.hamburg.tools.PlanParser;

/**
 * Demonstrates the use of {@link RecyclerView} with a {@link LinearLayoutManager} and a
 * {@link GridLayoutManager}.
 */
public class DishListFragmentImpl extends Fragment implements IDishListFragment {

    private static final String TAG = "DishListFragmentImpl";

    private static final String ARG_Day = "day";
    private static final String ARG_CODE = "code";
    private static String _lang;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeContainer;
    private Unbinder unbinder;
    private PlanParser planParser;
    @BindView(R.id.no_menu_or_offline)
    public TextView no_menu_or_offline;

    public static DishListFragmentImpl newInstance(int day, String code, String lang) {

        Bundle args = new Bundle();
        args.putInt(ARG_Day, day);
        args.putString(ARG_CODE, code);
        _lang = lang;

        DishListFragmentImpl fragment = new DishListFragmentImpl();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.recycler_view_frag, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        rootView.setTag(TAG);

        //TODO: This is currently not portable to butterknife cause the runnable, figure out why
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchData(getArguments().getInt(ARG_Day));
            }
        });
        swipeContainer.setColorSchemeResources(
                R.color.colorPrimaryDark,
                R.color.colorAccent,
                R.color.colorPrimary);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        fetchData(getArguments().getInt(ARG_Day));
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //The Task needs to be canceled before unbinding the views
        //otherwise a null pointer could happen
        planParser.cancel(true);
        unbinder.unbind();
    }

    @Override
    public void fetchData(int day) {
        planParser = new PlanParser(day, getArguments().getString("code"), _lang, new IPlanParserCallback() {
            @Override
            public void NoMenu() {
                isLoading(false);
                no_menu_or_offline.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }

            @Override
            public void SetDishes(ArrayList<Dish> dishList) {
                isLoading(false);
                setData(dishList);
                no_menu_or_offline.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
            }

            @Override
            public void ErrorWhileFetching() {
                isLoading(false);
            }
        });
        planParser.execute();
    }

    @Override
    public void setData(ArrayList<Dish> dishList){
        IMainPresenter mainPresenter = ((MainActivity) getActivity()).getActivityPresenter();
        DishAdapterImpl mAdapter = new DishAdapterImpl(dishList, mainPresenter);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void isLoading(final boolean isLoading) {
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(isLoading);
            }
        });
    }
}
