package de.android.mensa.hamburg.materials

class Allergenic @JvmOverloads constructor(val name: String?, private val _number: Int, val iconId: Int = 0) {

    override fun equals(o: Any?): Boolean {
        //If the other Object is null, it's for sure not equal
        if (o == null) {
            return false
        }
        //If the other object is neither a class nora subclass from this one, it is not equal
        if (!Allergenic::class.java.isAssignableFrom(o.javaClass)) {
            return false
        }
        //Since we know, that we have the same class, we can cast
        val tmp = o as Allergenic?
        //Onto checking the fields for equality
        //We can not equal against null, so we should not try if
        //this description is null
        if (this.name == null) {
            //If this description is null, the other should be too
            if (tmp!!.name != null) {
                return false
            }
        } else {
            //this description is not null, so we compare it against the other description
            if (this.name != tmp!!.name) {
                return false
            }
        }
        //After leaving the nasty null stuff behind us, we can jump to the yummy integers
        return !(this._number != tmp._number || this.iconId != tmp.iconId)
    }

    override fun hashCode(): Int {
        var hashCode = 42
        val multi = 29
        if (this.name != null) {
            hashCode += multi * this.name.hashCode()
        }
        hashCode += multi * _number
        hashCode += multi * iconId
        return hashCode
    }
}
