package de.android.mensa.hamburg;

import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

import de.android.mensa.hamburg.materials.Canteen;

interface IMainPresenter {
    /**
     * Get the view associated with the presenter
     * @return the view associated with the presenter
     */
    IMainView getView();

    /**
     * This is a "little" helper method for reoccurring Task,
     * which are needed in onResume and onCreate.
     * These are primarily things which could be changed in the settings (sprefs,canteens_list)
     * and which change while the app is suspended (isNetworkEnabled).
     */
    void setupAndRenew();

    /**
     * Checks if the network is accessible
     * @return if the network is accessible
     */
    boolean isNetworkEnabled();

    /**
     * Show the price for Students
     * @return if the price for students should be shown
     */
    boolean isShowStudentPrice();

    /**
     * Show the price for attendants
     * @return if the price for attendants should be shown
     */
    boolean isShowAttendantPrice();

    /**
     * Show the price for guests
     * @return if the price for guests should be shown
     */
    boolean isShowGuestPrice();

    /**
     * Show the icons for allergens
     * @return if the price for allergens should be shown
     */
    boolean isShownAllergens();

    /**
     * Get the size of the currently shown canteen list
     * @return the size of the currently shown canteen list
     */
    int getCanteensListSize();

    /**
     * Get a short title for a canteen
     * @param position the position of the canteen
     * @return the short title
     */
    CharSequence getShortNameForPosition(int position);

    /**
     * Return a specific fragment
     * @param position the position of the desired fragment
     * @return the desired fragment
     */
    Fragment returnSpecificFragment(int position);

    /**
     * Should be called, when the day spinner value was changed
     * @param day the value of the day spinner
     */
    void changedDayOnSpinner(int day);

    /**
     * This function handles incoming intents on the mainActivity.
     * Currently this could be either an url or the quicklaunch intent.
     *
     * @param intent The intent which should be handled
     */
    void handleIntent(Intent intent);

    /**
     * Get number many canteens from the canteens list, starting with the first
     * @param number how many canteens should be returned
     * @return an array list with the canteens
     */
    ArrayList<Canteen> getCanteensForDynamicShortcut(int number);

    /**
     * Opens and switch the tab to a specific canteen with the given code
     * If the canteen is not currently active, it is added
     *
     * @param code The code of the canteen, to which it should switch
     */
    void setAndOpenCanteenPerID(String code);

    /**
     * Adds and switch the tab to a specific canteen with the given code
     *
     * @param code The code of the canteen, which should be added and to which it should switch
     */
    void addCanteenToViewPager(String code);
}
