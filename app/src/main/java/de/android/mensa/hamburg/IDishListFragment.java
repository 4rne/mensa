package de.android.mensa.hamburg;

import java.util.ArrayList;

import de.android.mensa.hamburg.materials.Dish;

public interface IDishListFragment {
    /**
     * We ensure that the old data is deleted and the new data is set,
     * according to the day of the spinner
     *
     * @param day The day to be set (0 for today, 99 for next opening day)
     */
    void fetchData(int day);

    /**
     * Set a list of dishes in this fragment
     *
     * @param dishList the list of dishes to be set
     */
    void setData(ArrayList<Dish> dishList);

    /**
     * This is a not so beautiful solution tho show the loading indicator
     * If we only call setRefreshing, nothing happens
     *
     * @param isLoading If the Loding Indicator should be shown, or nor
     */
    void isLoading(final boolean isLoading);
}
