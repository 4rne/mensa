package de.android.mensa.hamburg;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

class AboutDialog extends AlertDialog {
    private static final String _email = "vhschlenker@gmail.com";
    private static final String _git = "https://gitlab.com/u/vhschlenker";
    private static final String _playstore = "https://play.google.com/store/apps/details?id=de.android.mensa.hamburg";

    @BindView(R.id.openReviewBtn)
    Button reviewBtn;
    @BindView(R.id.AppIcon)
    ImageView appIconIV;
    @BindView(R.id.AppName)
    TextView appNameTV;
    @BindView(R.id.AppVersion)
    TextView appVersionTV;
    @BindView(R.id.copyright)
    TextView copyrightTV;
    @BindView(R.id.email)
    TextView emailTV;
    @BindView(R.id.git)
    TextView gitTV;

    AboutDialog(final Context context) {
        super(context);

        PackageInfo pInfo;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            return;
        }

        Drawable icon = context.getPackageManager().getApplicationIcon(context.getApplicationInfo());
        String[] packageName = context.getPackageName().split("\\.");
        String name_location = capitalizeFirstCharOfString(packageName[2]) + " Uni " + capitalizeFirstCharOfString(packageName[3]);
        String version = pInfo.versionName;

        View aboutDialogView = getLayoutInflater().inflate(R.layout.about_dialog, null, false);
        ButterKnife.bind(this, aboutDialogView);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        appIconIV.setImageDrawable(icon);
        appNameTV.setText(name_location);
        appVersionTV.setText(version);
        copyrightTV.setText("Allergen icons by Lemon Liu\nfrom the Noun Project");
        emailTV.setText(_email);
        gitTV.setText(_git);

        reviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(_playstore));
                context.startActivity(browserIntent);
            }
        });

        builder.setNegativeButton("Close", null);
        builder.setView(aboutDialogView);
        builder.create();
        builder.show();
    }

    static String capitalizeFirstCharOfString(String string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }
}
