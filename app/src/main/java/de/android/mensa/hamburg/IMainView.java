package de.android.mensa.hamburg;

import android.content.Context;
import android.net.NetworkInfo;

interface IMainView {
    Context getAppContext();

    Context getActivityContext();

    /**
     * Display a "Device is offline" Toast
     */
    void showDeviceIsOffline();

    /**
     * Notify the Viewpager, that the canteen list has changed
     */
    void notifyCanteensListChanged();

    /**
     * Update the current selected item
     */
    void updateCurrentItemOnDateChange();

    /**
     * Set if the canteen viewpager should be scrollable
     * @param scrollable true if it should be scrollable, false if not
     */
    void setCanteenListScrollable(boolean scrollable);

    /**
     * Get the presenter associated with the view
     * @return the presenter associated with the view
     */
    IMainPresenter getActivityPresenter();

    /**
     * Set the pager to an item
     * @param index the index of the item
     */
    void setCurrentItem(int index);

    /**
     * Show an alert dialog
     * @param Message the message, which the alert dialog should display
     */
    void showAlertDialog(String Message);

    /**
     * Get the networkInfo
     * @return the network info
     */
    NetworkInfo getNetworkInfo();

    /**
     * Adds a list of canteens to the shortcuts on the launcher.
     * Currently, they are the first three entries of the canteen list.
     */
    void addDynamicShortcuts();
}
